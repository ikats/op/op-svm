"""
Copyright 2018 CS Systèmes d'Information

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""
from time import time
import numpy as np
from sklearn.svm import OneClassSVM as OneSVM
from ikatsbackend.core.resource.api import IkatsApi
import logging

LOGGER = logging.getLogger(__name__)
"""
    One-Class SVM - Fit part
    ========================
    Unsupervised Outlier Detection.
    Estimate the support of a high-dimensional distribution.
    The implementation is based on libsvm
"""


def one_svm_sklearn_fit(ts_list, kernel='rbf', degree=3, gamma='auto', coef=0.0, nu=0.5, random_state=None):
    """
    :param ts_list:
    :type ts_list:

    :param kernel: optional. Specifies the kernel type to be used in the algorithm. It must be one of ‘linear’, ‘poly’,
    ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable. If a callable is given it is used to precompute the kernel matrix.
    :type kernel: str

    :param degree: optional. Degree of the polynomial kernel function (‘poly’). Ignored by all other kernels.
    :type degree: int

    :param gamma: optional. Kernel coefficient for ‘rbf’, ‘poly’ and ‘sigmoid’.
    :type gamma: float

    :param coef: optional. Independent term in kernel function. It is only significant in ‘poly’ and ‘sigmoid’.
    :type coef: float

    :param nu: An upper bound on the fraction of training errors and a lower bound of the fraction of support vectors.
    Should be in the interval (0, 1].
    :type nu: float

    :param random_state: Used for TU
    :type random_state: int

    :return model_ocsvm:
    :rtype model_ocsvm:
    """
    LOGGER.info(" --- Starting One-Class SVM fit with scikit-learn --- ")
    # --------------------
    # 0 - Check the inputs
    # --------------------
    # Argument `ts_list`
    if type(ts_list) is not list:
        raise TypeError("Type of argument `ts_list` is {}, expected 'list'".format(type(ts_list)))
    elif not ts_list:
        raise ValueError("Argument `ts_list` is empty")
    # Other inputs are managed by the scikit learn function
    # ------------------------------
    # 0bis - Retrieve the tsuid list
    # ------------------------------
    try:
        tsuid_list = [x['tsuid'] for x in ts_list]
    except Exception:
        raise ValueError('Impossible to retrieve the tsuid list')
    try:
        start_time = time()
        # --------------------------------------------------------
        # 1 - Process the data in the shape needed by scikit-learn
        # --------------------------------------------------------
        # Extract input data (just data values, not timestamps)
        data = np.array(IkatsApi.ts.read(tsuid_list))[:, :, 1]
        # Shape = (n_ts, n_times)
        # Example: 4 TS with 2 times
        # array([[10., 9.],
        #        [1., 5.],
        #        [2., 6.],
        #        [12., 11.]])
        # -------------------------------
        # 2 - Fit the One-Class SVM model
        # -------------------------------
        model_ocsvm = OneSVM(kernel=kernel, degree=degree, gamma=gamma, coef0=coef, nu=nu, random_state=random_state)
        model_ocsvm.fit(data)
        return model_ocsvm
    finally:
        LOGGER.info(" --- Finished One-Class SVM fit with scikit-learn in: %.3f seconds --- ", time() - start_time)
