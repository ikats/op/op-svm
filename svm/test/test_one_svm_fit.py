"""
Copyright 2018 CS Systèmes d'Information

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import unittest
import numpy as np
import sklearn
import logging

from ikatsbackend.core.resource.api import IkatsApi
from ikatsbackend.algo.svm.one_svm_fit import one_svm_sklearn_fit

LOGGER = logging.getLogger()
# Log format. Used to be DEBUG
LOGGER.setLevel(logging.WARNING)
FORMATTER = logging.Formatter('%(asctime)s:%(levelname)s:%(funcName)s:%(message)s')
# Create another handler that will redirect log entries to STDOUT
STREAM_HANDLER = logging.StreamHandler()
# Used to be DEBUG
STREAM_HANDLER.setLevel(logging.WARNING)
STREAM_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(STREAM_HANDLER)


def gen_scenario(case):
    """
    Generate scenarios in order to perform UT.

    :param case: Allow to choose the generated scenario
    :type case: int

    :return:
        * my_clusters: A mock of the clustering obtained in the previous step
        * my_model: A mock of the K-Means model obtained in the previous step
        * my_old_ts: A mock of the TS used to build the K-Means model in the previous step
        * my_new_ts: The new set of TS to be assigned according to the K-Means model
    :rtype:
        * my_clusters: dict of dicts
        * my_model: sklearn.cluster.k_means_.KMeans
        * my_old_ts: list of dicts
        * my_new_ts: list of dicts
    """
    # -------------------
    # CASE 1: Normal case
    # -------------------
    if case == 1:
        # --- Generate the 4 new TS to be clustered according to the previous 'K-Means on TS' step ---
        ts_new = np.array(
            [
                np.array([
                    [14879030000, 10],
                    [14879031000, 9],
                ]),
                np.array([
                    [14879030000, 1],
                    [14879031000, 5],
                ]),
                np.array([
                    [14879030000, 2],
                    [14879031000, 6],
                ]),
                np.array([
                    [14879030000, 12],
                    [14879031000, 11],
                ])
            ], np.float64)

        # -------------------------------
        # Creation of the new TS in IKATS
        # -------------------------------
        my_ts = []
        for i in range(np.array(ts_new).shape[0]):
            # `fid` must be unique for each TS
            current_fid = 'UT_KMEANS_PRED_NEW_TS_' + str(i + 1)
            # Create TS
            created_ts = IkatsApi.ts.create(fid=current_fid, data=np.array(ts_new)[i, :, :])
            # Create metadata 'qual_ref_period'
            IkatsApi.md.create(tsuid=created_ts['tsuid'], name='qual_ref_period', value=1000, force_update=True)
            # Create metadata 'qual_nb_points'
            IkatsApi.md.create(tsuid=created_ts['tsuid'], name='qual_nb_points', value=2, force_update=True)
            # Check the 'status'
            if not created_ts['status']:
                raise SystemError("Error while creating TS %s" % current_fid)
            # Create a list of lists of TS (dicts)
            my_ts.append({'tsuid': created_ts['tsuid'], 'funcId': created_ts['funcId'], 'ts_content': ts_new[i]})
    else:
        raise NotImplementedError
    return my_ts


class TestOneSvmFit(unittest.TestCase):
    """
    Test of One-Class SVM on time series
    """
    @staticmethod
    def clean_up_db(ts_info):
        """
        Clean up the database by removing created TS
        :param ts_info: list of TS to remove
        """
        for ts_item in ts_info:
            # Delete created TS
            IkatsApi.ts.delete(tsuid=ts_item['tsuid'], no_exception=True)

    def test_arguments_one_svm_sklearn_fit(self):
        """
        Test the behaviour when wrong arguments are given on one_svm_sklearn_fit()
        """
        # Scenario generation to perform unit test
        my_ts = gen_scenario(case=1)
        try:
            # ------------------
            # Argument `ts_list`
            # ------------------
            # Wrong type (not a list)
            with self.assertRaises(TypeError, msg="Testing arguments: Error in testing `ts_list` type --- Wrong type"):
                # noinspection PyTypeChecker
                one_svm_sklearn_fit(ts_list=42, random_state=0)
            # Empty
            with self.assertRaises(ValueError, msg="Testing arguments: Error in testing `ts_list` type --- Empty"):
                one_svm_sklearn_fit(ts_list=[], random_state=0)
        finally:
            # Clean up database
            self.clean_up_db(my_ts)

    # ------------------ #
    # OUTPUTS TYPE TESTS #
    # ------------------ #
    def test_one_svm_sklearn_fit_output_type(self):
        """
        Test the type of the output of the one_svm_sklearn() function.
        """
        # Scenario generation to perform unit test
        my_ts = gen_scenario(1)
        try:
            # Test the wrapper
            model = one_svm_sklearn_fit(ts_list=my_ts, random_state=0)
            # Checks of output's type (dict)
            self.assertEqual(type(model), sklearn.svm.classes.OneClassSVM,
                             msg="ERROR: The output's type is not 'sklearn.svm.classes.OneClassSVM'")
        finally:
            # Clean up database
            self.clean_up_db(my_ts)
